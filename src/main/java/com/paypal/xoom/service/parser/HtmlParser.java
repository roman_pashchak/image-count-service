package com.paypal.xoom.service.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class HtmlParser {

    private static Logger LOGGER = LoggerFactory.getLogger(HtmlParser.class.getName());

    private Pattern imagePattern;

    public HtmlParser() {
        imagePattern = Pattern.compile("<img([\\w\\W]+?)[\\/]?>");
    }

    public int countImageTags(String urlValue) {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(urlValue).openStream()))) {
            int counter = 0;
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                Matcher matcher = imagePattern.matcher(inputLine);
                while (matcher.find()) {
                    counter++;
                }
            }
            return counter;
        } catch (MalformedURLException e) {
            LOGGER.error("can't parse resource due to malformed url: '{}'", urlValue, e);
        } catch (IOException e) {
            LOGGER.error("can't parse resource due to i/o error: '{}'", urlValue, e);
        }

        //TODO: return -2 that can be converted to {url1: "Error"}
        return 0;
    }

}
