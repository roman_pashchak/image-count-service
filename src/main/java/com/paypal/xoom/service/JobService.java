package com.paypal.xoom.service;

import com.paypal.xoom.exception.InvalidRequestException;
import com.paypal.xoom.exception.RecordNotFoundException;
import com.paypal.xoom.model.AcceptJobResponse;
import com.paypal.xoom.model.ViewJobResponse;
import com.paypal.xoom.service.parser.HtmlParser;
import com.paypal.xoom.service.task.JobTask;
import com.paypal.xoom.service.transformer.AcceptJobResponseTransformer;
import com.paypal.xoom.service.transformer.ViewJobResponseTransformer;
import com.paypal.xoom.storage.JobRecord;
import com.paypal.xoom.storage.JobRecordsStorage;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class JobService {

    public static final int THREAD_POOL_SIZE = 50;

    private HtmlParser htmlParser;
    private ExecutorService taskExecutor;
    private JobRecordsStorage jobRecordsStorage;
    private AcceptJobResponseTransformer acceptJobResponseTransformer;
    private ViewJobResponseTransformer viewJobResponseTransformer;


    public JobService() {
        this.jobRecordsStorage = new JobRecordsStorage();
        this.taskExecutor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        this.acceptJobResponseTransformer = new AcceptJobResponseTransformer();
        this.viewJobResponseTransformer = new ViewJobResponseTransformer();
        this.htmlParser = new HtmlParser();
    }

    protected JobService(HtmlParser htmlParser, ExecutorService taskExecutor, JobRecordsStorage jobRecordsStorage,
                         AcceptJobResponseTransformer acceptJobResponseTransformer, ViewJobResponseTransformer viewJobResponseTransformer) {
        this.htmlParser = htmlParser;
        this.taskExecutor = taskExecutor;
        this.jobRecordsStorage = jobRecordsStorage;
        this.acceptJobResponseTransformer = acceptJobResponseTransformer;
        this.viewJobResponseTransformer = viewJobResponseTransformer;
    }

    public AcceptJobResponse acceptJob(List<String> urls) {
        if (urls == null || urls.isEmpty()) {
            throw new InvalidRequestException("request must contain at least one url");
        }

        JobRecord jobRecord = new JobRecord(urls.size());
        jobRecordsStorage.store(jobRecord);

        for (int urlIndex = 0; urlIndex < urls.size(); urlIndex++) {
            String urlValue = urls.get(urlIndex);
            JobTask task = new JobTask(htmlParser, jobRecord, urlValue, urlIndex);
            taskExecutor.execute(task);
        }

        return acceptJobResponseTransformer.getAcceptJobResponse(jobRecord);
    }

    public ViewJobResponse getJob(String jobId) {
        JobRecord jobRecord = jobRecordsStorage.load(jobId);
        if (jobRecord == null) {
            throw new RecordNotFoundException("job not found");
        }

        return viewJobResponseTransformer.getViewJobResponse(jobRecord);
    }

    //cleanUp() is available for spring bean
    @PreDestroy
    private void cleanUp() {
        taskExecutor.shutdown();
    }

}
