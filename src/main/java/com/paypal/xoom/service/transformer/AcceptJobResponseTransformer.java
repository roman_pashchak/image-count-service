package com.paypal.xoom.service.transformer;

import com.paypal.xoom.model.AcceptJobResponse;
import com.paypal.xoom.storage.JobRecord;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class AcceptJobResponseTransformer {

    public AcceptJobResponse getAcceptJobResponse(JobRecord jobRecord) {
        return new AcceptJobResponse(jobRecord.getJobId());
    }
}
