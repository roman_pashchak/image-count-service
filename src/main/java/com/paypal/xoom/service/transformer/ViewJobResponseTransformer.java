package com.paypal.xoom.service.transformer;

import com.paypal.xoom.model.ViewJobResponse;
import com.paypal.xoom.storage.JobRecord;
import com.paypal.xoom.storage.JobStatus;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class ViewJobResponseTransformer {

    public ViewJobResponse getViewJobResponse(JobRecord jobRecord) {
        ViewJobResponse viewJobResponse = new ViewJobResponse(jobRecord.getJobId());
        for (int i = 0; i < jobRecord.getUrlsSize(); i++) {
            int imageCounter = jobRecord.getImageCounter(i);
            String urlName = String.format("url%d", i + 1);
            if (imageCounter != -1) {
                viewJobResponse.addImageCounter(urlName, imageCounter);
            } else {
                viewJobResponse.addImageCounter(urlName, JobStatus.PENDING);
            }
        }
        return viewJobResponse;
    }
}
