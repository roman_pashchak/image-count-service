package com.paypal.xoom.service.task;

import com.paypal.xoom.service.parser.HtmlParser;
import com.paypal.xoom.storage.JobRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class JobTask implements Runnable {

    private static Logger LOGGER = LoggerFactory.getLogger(JobTask.class.getName());

    private HtmlParser htmlParser;
    private JobRecord jobRecord;
    private String urlValue;
    private int urlIndex;

    public JobTask(HtmlParser htmlParser, JobRecord jobRecord, String urlValue, int urlIndex) {
        this.htmlParser = htmlParser;
        this.jobRecord = jobRecord;
        this.urlValue = urlValue;
        this.urlIndex = urlIndex;
    }

    @Override
    public void run() {
        LOGGER.info("worker #{} is processing url #{} '{}'", jobRecord.getJobId(), urlIndex, urlValue);

        int imageCounter = htmlParser.countImageTags(urlValue);

        jobRecord.setImageCounter(urlIndex, imageCounter);

        LOGGER.info("worker #{} found {} images in url #{} '{}'", jobRecord.getJobId(), imageCounter, urlIndex, urlValue);
    }
}
