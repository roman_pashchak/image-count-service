package com.paypal.xoom.controller;

import com.paypal.xoom.controller.helper.ResponseEntityHelper;
import com.paypal.xoom.model.AcceptJobResponse;
import com.paypal.xoom.model.ViewJobResponse;
import com.paypal.xoom.service.JobService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by roman.pashchak on 11/4/17.
 */
@RestController
@RequestMapping(value = "/counter")
public class JobController {

    private JobService jobService;

    @PostConstruct
    public void init() {
        this.jobService = new JobService();
    }

    public JobController() {
    }

    protected JobController(JobService jobService) {
        this.jobService = jobService;
    }

    @RequestMapping(value = "/job", method = RequestMethod.POST)
    public ResponseEntity acceptJob(@RequestBody List<String> urls) {
        AcceptJobResponse acceptJobResponse = jobService.acceptJob(urls);
        return ResponseEntityHelper.getEntity(acceptJobResponse);
    }


    @RequestMapping(value = "/job/{job_id}", method = RequestMethod.GET)
    public ResponseEntity getJob(@PathVariable(value = "job_id") String jobId) {
        ViewJobResponse viewJobResponse = jobService.getJob(jobId);
        return ResponseEntityHelper.getEntity(viewJobResponse);
    }


}
