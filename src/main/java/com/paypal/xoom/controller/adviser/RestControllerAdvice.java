package com.paypal.xoom.controller.adviser;

import com.paypal.xoom.exception.InvalidRequestException;
import com.paypal.xoom.exception.RecordNotFoundException;
import com.paypal.xoom.model.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by roman.pashchak on 11/4/17.
 */

@RestController
@ControllerAdvice(annotations = {RestController.class})
public class RestControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestControllerAdvice.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidRequestException.class)
    public ErrorResponse handleControllerException(InvalidRequestException e) {
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Request", e);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(RecordNotFoundException.class)
    public ErrorResponse handleControllerException(RecordNotFoundException e) {
        return new ErrorResponse(HttpStatus.NOT_FOUND.value(), "Not Found", e);
    }


    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable.class)
    public ErrorResponse handleControllerException(Throwable e) {
        LOGGER.error("Exception caught: {}", e.getMessage(), e);
        return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Service Exception", e);
    }


}
