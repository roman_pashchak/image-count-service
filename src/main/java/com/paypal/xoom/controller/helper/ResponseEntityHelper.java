package com.paypal.xoom.controller.helper;

import com.paypal.xoom.model.AcceptJobResponse;
import com.paypal.xoom.model.ViewJobResponse;
import com.paypal.xoom.storage.JobStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class ResponseEntityHelper {


    public static ResponseEntity getEntity(AcceptJobResponse acceptJobResponse) {
        return new ResponseEntity(acceptJobResponse, HttpStatus.OK);
    }

    public static ResponseEntity getEntity(ViewJobResponse viewJobResponse) {
        HttpStatus status = HttpStatus.OK;

        boolean anyPendingUrl = viewJobResponse.getImageCounters().values()
                .stream()
                .anyMatch(value -> JobStatus.PENDING.equals(value));
        if (anyPendingUrl) {
            status = HttpStatus.PARTIAL_CONTENT;
        }
        return new ResponseEntity(viewJobResponse, status);
    }
}
