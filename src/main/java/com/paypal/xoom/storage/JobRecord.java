package com.paypal.xoom.storage;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class JobRecord {

    private String jobId;
    private int urlsSize;
    private AtomicIntegerArray imageCounters;

    public JobRecord(int urlsSize) {
        this.jobId = UUID.randomUUID().toString();
        this.urlsSize = urlsSize;

        int[] initImageCounters = new int[urlsSize];
        Arrays.fill(initImageCounters, -1);
        this.imageCounters = new AtomicIntegerArray(initImageCounters);
    }


    public String getJobId() {
        return jobId;
    }

    public void setImageCounter(int urlIndex, int imageCounter) {
        this.imageCounters.set(urlIndex, imageCounter);
    }

    public int getUrlsSize() {
        return this.urlsSize;
    }

    public int getImageCounter(int urlIndex) {
        return this.imageCounters.get(urlIndex);
    }

}