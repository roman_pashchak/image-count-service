package com.paypal.xoom.storage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class JobRecordsStorage {

    protected Map<String, JobRecord> records; // by id

    public JobRecordsStorage() {
        this.records = new ConcurrentHashMap<>();
    }

    public JobRecord load(String jobId) {
        return records.get(jobId);
    }

    public void store(JobRecord jobRecord) {
        records.put(jobRecord.getJobId(), jobRecord);
    }

}
