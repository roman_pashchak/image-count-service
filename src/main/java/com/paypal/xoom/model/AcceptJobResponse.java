package com.paypal.xoom.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class AcceptJobResponse implements Serializable {

    @JsonProperty("Job-Id")
    private String jobId;

    public AcceptJobResponse(String jobId) {
        this.jobId = jobId;
    }

    public String getJobId() {
        return jobId;
    }

}
