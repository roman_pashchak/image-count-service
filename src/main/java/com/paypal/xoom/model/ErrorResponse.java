package com.paypal.xoom.model;

import java.io.Serializable;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class ErrorResponse implements Serializable {

    private Integer responseCode;

    private String errorType;

    private String message;

    public ErrorResponse() {

    }

    public ErrorResponse(Integer responseCode, String errorType, Throwable throwable) {
        this.responseCode = responseCode;
        this.errorType = errorType;
        this.message = throwable.getMessage();
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
