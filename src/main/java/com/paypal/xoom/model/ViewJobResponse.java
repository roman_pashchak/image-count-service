package com.paypal.xoom.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class ViewJobResponse implements Serializable {
    
    @JsonProperty("Job-Id")
    private String jobId;

    @JsonProperty("urls")
    public Map<String, Object> imageCounters = new LinkedHashMap<>();

    public ViewJobResponse(String jobId) {
        this.jobId = jobId;
    }


    public Map<String, Object> getImageCounters() {
        return imageCounters;
    }

    public void addImageCounter(String urlName, Object value) {
        this.imageCounters.put(urlName, value);
    }

    public String getJobId() {
        return jobId;
    }

}
