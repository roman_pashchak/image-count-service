package com.paypal.xoom.exception;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class RecordNotFoundException extends InvalidRequestException {

    public RecordNotFoundException(String message) {
        super(message);
    }
}
