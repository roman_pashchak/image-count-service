package com.paypal.xoom.exception;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class InvalidRequestException extends RuntimeException {

    public InvalidRequestException(String message) {
        super(message);
    }
}
