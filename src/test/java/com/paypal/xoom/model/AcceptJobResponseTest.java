package com.paypal.xoom.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class AcceptJobResponseTest {

    @Test
    public void testConstructorSetsJobId(){
        AcceptJobResponse acceptJobResponse = new AcceptJobResponse("job-id-1");
        assertEquals("job-id-1",acceptJobResponse.getJobId());
    }
}
