package com.paypal.xoom.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class ViewJobResponseTest {

    @Test
    public void testConstructorSetsJobIdAndEmptyImageCounters() {
        ViewJobResponse viewJobResponse = new ViewJobResponse("job-id-1");
        assertEquals("job-id-1", viewJobResponse.getJobId());
        assertNotNull(viewJobResponse.getImageCounters());
        assertTrue(viewJobResponse.getImageCounters().isEmpty());
    }

    @Test
    public void testAddImageCounterSetsValue() {
        ViewJobResponse viewJobResponse = new ViewJobResponse("job-id-1");

        viewJobResponse.addImageCounter("url1", 100);
        viewJobResponse.addImageCounter("url2", "Pending");

        assertEquals("job-id-1", viewJobResponse.getJobId());
        assertNotNull(viewJobResponse.getImageCounters());
        assertEquals(2, viewJobResponse.getImageCounters().size());
        assertEquals(100, viewJobResponse.getImageCounters().get("url1"));
        assertEquals("Pending", viewJobResponse.getImageCounters().get("url2"));
    }
}
