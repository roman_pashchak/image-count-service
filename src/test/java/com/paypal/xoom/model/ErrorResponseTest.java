package com.paypal.xoom.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class ErrorResponseTest {

    @Test
    public void testConstructorSetsJobId() {
        Throwable throwable = mock(Throwable.class);
        when(throwable.getMessage()).thenReturn("throwable message");

        ErrorResponse errorResponse = new ErrorResponse(404, "error-type", throwable);

        assertNotNull(errorResponse.getResponseCode());
        assertEquals(404, errorResponse.getResponseCode().intValue());
        assertEquals("error-type", errorResponse.getErrorType());
        assertEquals("throwable message", errorResponse.getMessage());
    }
}
