package com.paypal.xoom.storage;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class JobRecordTest {

    private JobRecord jobRecord;

    @Before
    public void setUp() {
        jobRecord = new JobRecord(2);
    }

    @Test
    public void testJobIdIsNotEmpty() {
        assertNotNull(jobRecord.getJobId());
        assertFalse(jobRecord.getJobId().isEmpty());
    }

    @Test
    public void testGetImageCounterReturnsDefaultValue() {
        assertEquals(-1, jobRecord.getImageCounter(0));
        assertEquals(-1, jobRecord.getImageCounter(1));
    }

    @Test
    public void testSetImageCounterSetsValue() {
        jobRecord.setImageCounter(0, 100);
        jobRecord.setImageCounter(1, 200);

        assertEquals(100, jobRecord.getImageCounter(0));
        assertEquals(200, jobRecord.getImageCounter(1));
    }

}
