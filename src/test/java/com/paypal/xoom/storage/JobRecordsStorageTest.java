package com.paypal.xoom.storage;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class JobRecordsStorageTest {

    private JobRecordsStorage jobRecordsStorage;

    //mock
    private JobRecord jobRecord1;
    private JobRecord jobRecord2;

    @Before
    public void setUp() {
        jobRecord1 = mock(JobRecord.class);
        jobRecord2 = mock(JobRecord.class);

        when(jobRecord1.getJobId()).thenReturn("job-id-1");
        when(jobRecord2.getJobId()).thenReturn("job-id-2");

        jobRecordsStorage = new JobRecordsStorage();
    }

    @Test
    public void testStoreAndLoadJobRecords() {
        jobRecordsStorage.store(jobRecord1);
        jobRecordsStorage.store(jobRecord2);

        assertEquals(jobRecord1, jobRecordsStorage.load("job-id-1"));
        assertEquals(jobRecord2, jobRecordsStorage.load("job-id-2"));
    }

    @Test
    public void testLoadReturnsNullJobRecordIfIdNotPresent() {
        assertNull(jobRecordsStorage.load("job-id-3"));
    }
}
