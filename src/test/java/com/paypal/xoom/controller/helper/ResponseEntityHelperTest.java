package com.paypal.xoom.controller.helper;

import com.paypal.xoom.model.AcceptJobResponse;
import com.paypal.xoom.model.ViewJobResponse;
import com.paypal.xoom.storage.JobStatus;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class ResponseEntityHelperTest {


    @Test
    public void testGetEntityReturnsOkResponseEntityIfAcceptJobResponse() {
        AcceptJobResponse acceptJobResponse = mock(AcceptJobResponse.class);

        ResponseEntity entity = ResponseEntityHelper.getEntity(acceptJobResponse);
        assertNotNull(entity);
        assertEquals(acceptJobResponse, entity.getBody());
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(HttpStatus.OK.value(), entity.getStatusCodeValue());
    }


    @Test
    public void testGetEntityReturnsPartialResponseEntityIfPendingViewJobResponse() {
        Map<String, Object> imageCounters = new LinkedHashMap<>();
        imageCounters.put("url1", 100);
        imageCounters.put("url2", JobStatus.PENDING);

        ViewJobResponse viewJobResponse = mock(ViewJobResponse.class);
        when(viewJobResponse.getImageCounters()).thenReturn(imageCounters);

        ResponseEntity entity = ResponseEntityHelper.getEntity(viewJobResponse);
        assertNotNull(entity);
        assertEquals(viewJobResponse, entity.getBody());
        assertEquals(HttpStatus.PARTIAL_CONTENT, entity.getStatusCode());
        assertEquals(HttpStatus.PARTIAL_CONTENT.value(), entity.getStatusCodeValue());
    }

    @Test
    public void testGetEntityReturnsOkResponseEntityIfCompletedViewJobResponse() {
        Map<String, Object> imageCounters = new LinkedHashMap<>();
        imageCounters.put("url1", 100);
        imageCounters.put("url2", 200);

        ViewJobResponse viewJobResponse = mock(ViewJobResponse.class);
        when(viewJobResponse.getImageCounters()).thenReturn(imageCounters);

        ResponseEntity entity = ResponseEntityHelper.getEntity(viewJobResponse);
        assertNotNull(entity);
        assertEquals(viewJobResponse, entity.getBody());
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(HttpStatus.OK.value(), entity.getStatusCodeValue());
    }
}
