package com.paypal.xoom.controller;

import com.paypal.xoom.controller.helper.ResponseEntityHelper;
import com.paypal.xoom.model.AcceptJobResponse;
import com.paypal.xoom.model.ViewJobResponse;
import com.paypal.xoom.service.JobService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by roman.pashchak on 11/4/17.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ResponseEntityHelper.class})
public class JobControllerTest {

    private JobController jobController;

    //mock
    private JobService jobService;

    @Before
    public void setUp() {
        jobService = mock(JobService.class);

        PowerMockito.mockStatic(ResponseEntityHelper.class);

        jobController = new JobController(jobService);
    }

    @Test
    public void testAcceptJobCallsJobServiceAndReturnsCorrectResponseEntity() throws Exception {
        AcceptJobResponse acceptJobResponse = mock(AcceptJobResponse.class);
        List<String> urls = mock(List.class);
        when(jobService.acceptJob(urls)).thenReturn(acceptJobResponse);


        ResponseEntity acceptJobResponseEntity = mock(ResponseEntity.class);
        PowerMockito.when(ResponseEntityHelper.class, "getEntity", acceptJobResponse).thenReturn(acceptJobResponseEntity);

        //test
        ResponseEntity responseEntity = jobController.acceptJob(urls);
        assertNotNull(responseEntity);
        assertEquals(acceptJobResponseEntity, responseEntity);
    }

    @Test
    public void testGetJobCallsJobServiceAndReturnsCorrectResponseEntity() throws Exception {
        ViewJobResponse viewJobResponse = mock(ViewJobResponse.class);
        when(jobService.getJob("job-id-1")).thenReturn(viewJobResponse);

        ResponseEntity viewJobResponseEntity = mock(ResponseEntity.class);
        PowerMockito.when(ResponseEntityHelper.class, "getEntity", viewJobResponse).thenReturn(viewJobResponseEntity);

        //test
        ResponseEntity responseEntity = jobController.getJob("job-id-1");
        assertNotNull(responseEntity);
        assertEquals(viewJobResponseEntity, responseEntity);
    }
}
