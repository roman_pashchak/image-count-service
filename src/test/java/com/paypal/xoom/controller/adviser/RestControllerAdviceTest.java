package com.paypal.xoom.controller.adviser;

import com.paypal.xoom.exception.InvalidRequestException;
import com.paypal.xoom.exception.RecordNotFoundException;
import com.paypal.xoom.model.ErrorResponse;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class RestControllerAdviceTest {

    private RestControllerAdvice restControllerAdvice;


    @Before
    public void setUp() {
        restControllerAdvice = new RestControllerAdvice();
    }


    @Test
    public void testHandleControllerExceptionReturnsErrorResponseIfInvalidRequestException() {
        InvalidRequestException invalidRequestException = mock(InvalidRequestException.class);
        when(invalidRequestException.getMessage()).thenReturn("invalid request message");

        ErrorResponse errorResponse = restControllerAdvice.handleControllerException(invalidRequestException);

        assertNotNull(errorResponse);
        assertEquals("invalid request message", errorResponse.getMessage());
        assertEquals("Invalid Request", errorResponse.getErrorType());
        assertNotNull(errorResponse.getResponseCode());
        assertEquals(HttpStatus.BAD_REQUEST.value(), errorResponse.getResponseCode().intValue());
    }

    @Test
    public void testHandleControllerExceptionReturnsErrorResponseIfRecordNotFoundException() {
        RecordNotFoundException recordNotFoundException = mock(RecordNotFoundException.class);
        when(recordNotFoundException.getMessage()).thenReturn("not found message");

        ErrorResponse errorResponse = restControllerAdvice.handleControllerException(recordNotFoundException);

        assertNotNull(errorResponse);
        assertEquals("not found message", errorResponse.getMessage());
        assertEquals("Not Found", errorResponse.getErrorType());
        assertNotNull(errorResponse.getResponseCode());
        assertEquals(HttpStatus.NOT_FOUND.value(), errorResponse.getResponseCode().intValue());
    }

    @Test
    public void testHandleControllerExceptionReturnsErrorResponseIfThrowable() {
        Throwable throwable = mock(Throwable.class);
        when(throwable.getMessage()).thenReturn("throwable message");

        ErrorResponse errorResponse = restControllerAdvice.handleControllerException(throwable);

        assertNotNull(errorResponse);
        assertEquals("throwable message", errorResponse.getMessage());
        assertEquals("Service Exception", errorResponse.getErrorType());
        assertNotNull(errorResponse.getResponseCode());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), errorResponse.getResponseCode().intValue());
    }

}
