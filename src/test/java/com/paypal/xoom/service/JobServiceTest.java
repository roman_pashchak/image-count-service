package com.paypal.xoom.service;

import com.paypal.xoom.exception.InvalidRequestException;
import com.paypal.xoom.model.AcceptJobResponse;
import com.paypal.xoom.model.ViewJobResponse;
import com.paypal.xoom.service.parser.HtmlParser;
import com.paypal.xoom.service.task.JobTask;
import com.paypal.xoom.service.transformer.AcceptJobResponseTransformer;
import com.paypal.xoom.service.transformer.ViewJobResponseTransformer;
import com.paypal.xoom.storage.JobRecord;
import com.paypal.xoom.storage.JobRecordsStorage;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by roman.pashchak on 11/5/17.
 */
public class JobServiceTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private JobService jobService;

    //mock
    private HtmlParser htmlParser;
    private ExecutorService taskExecutor;
    private JobRecordsStorage jobRecordsStorage;
    private AcceptJobResponseTransformer acceptJobResponseTransformer;
    private ViewJobResponseTransformer viewJobResponseTransformer;

    @Before
    public void setUp() {
        htmlParser = mock(HtmlParser.class);
        taskExecutor = mock(ExecutorService.class);
        jobRecordsStorage = mock(JobRecordsStorage.class);
        acceptJobResponseTransformer = mock(AcceptJobResponseTransformer.class);
        viewJobResponseTransformer = mock(ViewJobResponseTransformer.class);

        jobService = new JobService(htmlParser, taskExecutor, jobRecordsStorage, acceptJobResponseTransformer, viewJobResponseTransformer);
    }

    @Test
    public void testAcceptJobThrowsExceptionIfNullUrls() {
        thrown.expect(InvalidRequestException.class);
        thrown.expectMessage("request must contain at least one url");

        jobService.acceptJob(null);
    }

    @Test
    public void testAcceptJobThrowsExceptionIfEmptyUrls() {
        thrown.expect(InvalidRequestException.class);
        thrown.expectMessage("request must contain at least one url");

        jobService.acceptJob(new ArrayList<>());
    }


    @Test
    public void testAcceptJobCreatesNewRecordAndRunsTasks() {

        AcceptJobResponse acceptJobResponse = mock(AcceptJobResponse.class);
        when(acceptJobResponseTransformer.getAcceptJobResponse(any())).thenReturn(acceptJobResponse);

        AcceptJobResponse response = jobService.acceptJob(Arrays.asList("url://1", "url://2", "url://2"));

        assertNotNull(response);
        assertEquals(acceptJobResponse, response);

        verify(jobRecordsStorage).store(any());
        verify(taskExecutor, times(3)).execute(any(JobTask.class));
        verify(acceptJobResponseTransformer).getAcceptJobResponse(any());
    }


    @Test
    public void testGetJobThrowsExceptionIfRecordNotPresent() {
        thrown.expect(InvalidRequestException.class);
        thrown.expectMessage("job not found");

        when(jobRecordsStorage.load("job-id-2")).thenReturn(null);
        jobService.getJob("job-id-2");
    }


    @Test
    public void testGetJobLoadsJobFromStorage() {
        JobRecord jobRecord = mock(JobRecord.class);
        ViewJobResponse viewJobResponse = mock(ViewJobResponse.class);

        when(jobRecordsStorage.load("job-id-1")).thenReturn(jobRecord);
        when(viewJobResponseTransformer.getViewJobResponse(jobRecord)).thenReturn(viewJobResponse);

        ViewJobResponse response = jobService.getJob("job-id-1");

        assertNotNull(response);
        assertEquals(viewJobResponse, response);

        verify(jobRecordsStorage).load("job-id-1");
        verify(viewJobResponseTransformer).getViewJobResponse(jobRecord);
    }

}
