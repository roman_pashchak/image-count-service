package com.paypal.xoom.service.transformer;

import com.paypal.xoom.model.AcceptJobResponse;
import com.paypal.xoom.storage.JobRecord;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class AcceptJobResponseTransformerTest {

    private AcceptJobResponseTransformer acceptJobResponseTransformer;

    //mock
    private JobRecord jobRecord;

    @Before
    public void setUp() {
        jobRecord = mock(JobRecord.class);
        when(jobRecord.getJobId()).thenReturn("job-id-1");

        acceptJobResponseTransformer = new AcceptJobResponseTransformer();
    }

    @Test
    public void testGetAcceptJobResponseSetsJobId() {
        AcceptJobResponse response = acceptJobResponseTransformer.getAcceptJobResponse(jobRecord);

        assertNotNull(response);
        assertEquals("job-id-1", response.getJobId());
    }
}
