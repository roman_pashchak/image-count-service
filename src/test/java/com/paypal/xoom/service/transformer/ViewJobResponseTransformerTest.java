package com.paypal.xoom.service.transformer;

import com.paypal.xoom.model.ViewJobResponse;
import com.paypal.xoom.storage.JobRecord;
import com.paypal.xoom.storage.JobStatus;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class ViewJobResponseTransformerTest {

    private ViewJobResponseTransformer viewJobResponseTransformer;

    //mock
    private JobRecord jobRecord;

    @Before
    public void setUp() {
        jobRecord = mock(JobRecord.class);
        when(jobRecord.getJobId()).thenReturn("job-id-1");
        when(jobRecord.getUrlsSize()).thenReturn(3);
        when(jobRecord.getImageCounter(0)).thenReturn(100);
        when(jobRecord.getImageCounter(1)).thenReturn(-1);
        when(jobRecord.getImageCounter(2)).thenReturn(200);

        viewJobResponseTransformer = new ViewJobResponseTransformer();
    }

    @Test
    public void testGetViewJobResponseSetsJobIdAndImageCounters() {
        ViewJobResponse response = viewJobResponseTransformer.getViewJobResponse(jobRecord);

        assertNotNull(response);
        assertEquals("job-id-1", response.getJobId());

        assertNotNull(response.getImageCounters());
        assertEquals(3, response.getImageCounters().size());
        assertEquals(100, response.getImageCounters().get("url1"));
        assertEquals(JobStatus.PENDING, response.getImageCounters().get("url2"));
        assertEquals(200, response.getImageCounters().get("url3"));
    }

}
