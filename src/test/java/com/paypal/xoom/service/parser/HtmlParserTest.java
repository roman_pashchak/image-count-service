package com.paypal.xoom.service.parser;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class HtmlParserTest {

    private static String urlValue;

    private HtmlParser htmlParser;

    @Before
    public void setUp() {
        htmlParser = new HtmlParser();
    }

    @BeforeClass
    public static void globalSetup() throws Exception {
        urlValue = new File("src/test/resources/view-source_https___en.wikipedia.org_wiki_Max_Levchin.htm").toURI().toURL().toString();
    }

    @Test
    public void testCountImageTagsReturnsCorrectCounter() {
        int counter = htmlParser.countImageTags(urlValue);
        assertEquals(6, counter);
    }

    @Test
    public void testCountImageTagsReturnsZeroIfIOException() {
        int counter = htmlParser.countImageTags("file:/file-not-found-path");
        assertEquals(0, counter);
    }
}
