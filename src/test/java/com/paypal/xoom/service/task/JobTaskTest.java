package com.paypal.xoom.service.task;

import com.paypal.xoom.service.parser.HtmlParser;
import com.paypal.xoom.storage.JobRecord;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by roman.pashchak on 11/4/17.
 */
public class JobTaskTest {

    private JobTask jobTask;

    //mock
    private HtmlParser htmlParser;
    private JobRecord jobRecord;

    @Before
    public void setUp() {
        htmlParser = mock(HtmlParser.class);
        jobRecord = mock(JobRecord.class);

        when(htmlParser.countImageTags("url-value")).thenReturn(1012);

        jobTask = new JobTask(htmlParser, jobRecord, "url-value", 56);
    }

    @Test
    public void testJobTaskIsInstanceOfRunable() {
        assertTrue(jobTask instanceof Runnable);
    }

    @Test
    public void testRunCallsParserAndStoresCounterIsJobRecord() {
        jobTask.run();

        verify(htmlParser).countImageTags("url-value");
        verify(jobRecord).setImageCounter(56, 1012);
    }
}
